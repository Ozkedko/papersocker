#ifndef SOCKERPOINT_H
#define SOCKERPOINT_H

#include <QLine>
#include <QList>
#include <vector>
#include <QDebug>

using namespace std;

enum Directions // ����������� �������� �� ������ �����
{
    EAST,
    NORTH_EAST,
    NORTH,
    WEST_NORTH,
    WEST,
    SOUTH_WEST,
    SOUTH,
    SOUTH_EAST,
    WRONG_DIRECTION
};

/*******************************�������� ������******************************
 *                                                                          *
 *      ������ ����� �������� ������� ����� �� ����, ������� ����� �����    *
 *  ������������ �������� ����� ���:                                        *
 *          - ����������                                                    *
 *          - ������� �������                                               *
 *          - �������� �� ��� ����� ��������                                *
 *      � ����������� �� ����������� ����� ������ ����� ��������� �������   *
 *  ����.                                                                   *
 *                                                                          *
 ****************************************************************************/


class SockerPoint
{
    //-----------------------�����������-------------------------
    public:
        SockerPoint();
        SockerPoint(int x, int y);
        ~SockerPoint();

    //-----------------------����������--------------------------
        const static int COURSES = 8;

    //-----------------------����������--------------------------
    private:
        bool PossibleCourses[COURSES + 1];
                /* ������� �������(���� ����� ������) */


        bool Gate;
                /* ��� ����� �������� �������� */

        int  X;
        int  Y; /* ���������� ���� ����� */


    //-----------------------������------------------------------
    public:
        bool isCorrectCourse(int x, int y);
                /* ���������� ����������� �������� */

        QVector<QLine> Lines;
                /* ������ �����, ������� �� ����� ����� ���� */

        bool isGate();
                /* ��� ������? */

        bool isSteped();
                /* ���� ��� ������(���������)?*/

        void closeCourse(int x, int y);
                /* ������� ����������� �������� */

        void setX(int x);
        void setY(int y);
        int  getX();
        int  getY();
                /* �������� ���������� */

        void setGate(bool isgate);
                /* �������, ��� ��� ����� �������� ��������*/

        int getCountOfFreedom();
                /* ���������� ������� ������� - ���������� ��������� ����������� */

        vector<QLine> getLines();
                /* ���������� ���������� �����, �� ������� ������ �� ���� ����� */

    private:
        int getCourse(int x, int y);
                /* ������ �������� ������� ��������� �������� �������
                 * ������������� ������ �����������. */

};
#endif // SOCKERPOINT_H
