#ifndef SOCKERTYPES_H
#define SOCKERTYPES_H

#include "sockerpoint.h"

#define ASCET_FIELD_HEIGHT  10 // ����������� ������ ���� �� ������ ��������� �.
#define ASCET_FIELD_WIDTH   20 // ����������� ������ ���� �� ������ ��������� �.

#define RNR_FIELD_HEIGHT    6  // ����������� ������ ���� �� ������ ��������� �.
#define RNR_FIELD_WIDTH     8  // ����������� ������ ���� �� ������ ��������� �.

namespace SockerTypes
{
    typedef enum
    {
        AscetField,
        RnrField,
        OtherField
    }FieldTypes;

    typedef enum
    {
        HumanPlayer,
        AIPlayer,
        RemotePlayer
    }PlayerTypes;
}



#endif // SOCKERTYPES_H
