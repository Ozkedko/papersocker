#include "sockerpoint.h"

SockerPoint::SockerPoint():Gate(false)
{
    for(int i = 0; i < COURSES; i++)
    {
        PossibleCourses[i] = true;
    }
    PossibleCourses[WRONG_DIRECTION] = false;
}

SockerPoint::SockerPoint(int x, int y):
                        Gate(false),
                        X(x),
                        Y(y)

{
    for(int i = 0; i < COURSES; i++)
    {
        PossibleCourses[i] = true;
    }
}

SockerPoint::~SockerPoint()
{

}

bool SockerPoint::isCorrectCourse(int x, int y)
{
    return PossibleCourses[getCourse(x, y)];
}

bool SockerPoint::isGate()
{
    return Gate;
}

bool SockerPoint::isSteped()
{
    for(int i = 0; i < COURSES; i++)
    {
        if(PossibleCourses[i] == false)
        {
            return true;
        }
    }
    return false;
}

void SockerPoint::closeCourse(int x, int y)
{
    PossibleCourses[getCourse(x, y)] = false;
}

void SockerPoint::setX(int x)
{
    X = x;
}

void SockerPoint::setY(int y)
{
    Y = y;
}

int SockerPoint::getX()
{
    return X;
}

int SockerPoint::getY()
{
    return Y;
}

void SockerPoint::setGate(bool isgate)
{
    Gate = isgate;
}

int SockerPoint::getCountOfFreedom()
{
    int count = 0;
    for(int i = 0; i < COURSES; i++)
    {
        if(PossibleCourses[i] == true)
        {
            count++;
        }
    }
    return count;
}

vector<QLine> SockerPoint::getLines()
{
    Lines.clear();
    for(int i = 0; i < COURSES; i++)
    {
        if(PossibleCourses[i] == false)
        {
            switch(i)
            {
                case EAST:      {Lines.push_back(QLine(X, Y, X + 1, Y    )); break;}
                case NORTH_EAST:{Lines.push_back(QLine(X, Y, X + 1, Y - 1)); break;}
                case NORTH:     {Lines.push_back(QLine(X, Y, X,     Y - 1)); break;}
                case WEST_NORTH:{Lines.push_back(QLine(X, Y, X - 1, Y - 1)); break;}
                case WEST:      {Lines.push_back(QLine(X, Y, X - 1, Y    )); break;}
                case SOUTH_WEST:{Lines.push_back(QLine(X, Y, X - 1, Y + 1)); break;}
                case SOUTH:     {Lines.push_back(QLine(X, Y, X ,    Y + 1)); break;}
                case SOUTH_EAST:{Lines.push_back(QLine(X, Y, X + 1, Y + 1)); break;}
            }
        }
    }

    return Lines.toStdVector();
}

int SockerPoint::getCourse(int x, int y)
{
    if((x > getX())  && (y == getY())){return EAST;      }
    if((x > getX())  && (y < getY())) {return NORTH_EAST;}
    if((x == getX()) && (y < getY())) {return NORTH;     }
    if((x < getX())  && (y < getY())) {return WEST_NORTH;}
    if((x < getX())  && (y == getY())){return WEST;      }
    if((x < getX())  && (y > getY())) {return SOUTH_WEST;}
    if((x == getX()) && (y > getY())) {return SOUTH;     }
    if((x > getX())  && (y > getY())) {return SOUTH_EAST;}
    return WRONG_DIRECTION;
}

