#include "sockerfield.h"

SockerField::SockerField(int width, int height, QObject* parent):
    QObject(parent),
    Width(width),
    Height(height)
{
    generateField(OtherField);
}

SockerField::SockerField(FieldTypes type, QObject* parent): QObject(parent)
{
    generateField(type);
}

SockerField::~SockerField()
{
    delete [] Field;
}
int SockerField::getHeight() const
{
    return Height;
}

void SockerField::setHeight(int value)
{
    Height = value;
}

int SockerField::getWidth() const
{
    return Width;
}

void SockerField::setWidth(int value)
{
    Width = value;
}


bool SockerField::isValidCouse(int x, int y)
{
    QLine newStep(Ball->getX(), Ball->getY(), x, y);
    for (vector<QLine>::const_iterator it = Steps.begin(); it != Steps.end(); ++it)
    {
        if(*it == newStep)
        {
            return false;
        }
    }

    return (Ball->isCorrectCourse(x, y) && !isInvertStep(Ball->getX(), Ball->getY(), x, y));
}

bool SockerField::isMoveAgain()
{
    return ((Ball->getCountOfFreedom() != 8) || (Steps.empty()));
            /* ���� ���� ������, �� �� ��������, �� ������� ������� 7,
             * � ���� ������� ���� ��� ��� */
}

void SockerField::generateField(FieldTypes type)
{
    switch(type)
    {
        case AscetField:
        {
            Width = ASCET_FIELD_WIDTH;
            Height = ASCET_FIELD_HEIGHT;

            initField();
            break;
        }
        case RnrField:
        {
            Width = RNR_FIELD_WIDTH;
            Height = RNR_FIELD_HEIGHT;

            initField();

            for(int y = 0; y < Height; y++)
            {       /* �������� ������������ ����� ����� � ������ �� ���� */
                Field[Ball->getX() - 1][y].closeCourse(Ball->getX() - 1, y - 1);
                Field[Ball->getX() - 1][y].closeCourse(Ball->getX() - 1, y + 1);

                Field[Ball->getX() + 1][y].closeCourse(Ball->getX() + 1, y - 1);
                Field[Ball->getX() + 1][y].closeCourse(Ball->getX() + 1, y + 1);
            }
            break;
        }
        case OtherField:
        {
            initField();
        }
    }
}

void SockerField::initField()
{
    Field = new SockerPoint*[Width + 1];

    for(int i = 0; i < Width + 1; i++)
    {
        Field[i] = new SockerPoint[Height + 1];
    }

    for(int x = 0; x <= Width; x++)
    {
        for(int y = 0; y <= Height; y++)
        {
            Field[x][y].setX(x);
            Field[x][y].setY(y);

            if(x == 0) // ���� ����� ����
            {
                Field[x][y].closeCourse(x    , y - 1);   //    |
                Field[x][y].closeCourse(x - 1, y - 1);   /*   \    */
                Field[x][y].closeCourse(x - 1, y    );   //   -*
                Field[x][y].closeCourse(x - 1, y + 1);   //   /
                Field[x][y].closeCourse(x    , y + 1);   //    |
                        /* ��������� ����������� ����� �� ���� ���� � ����� �����
                         * ������������ ���������� �����������, ������� ��������� */
            }

            if(x == Width) // ���� ������ ����
            {
                Field[x][y].closeCourse(x    , y - 1);  // |
                Field[x][y].closeCourse(x + 1, y - 1);  //  /
                Field[x][y].closeCourse(x + 1, y    );  // *-
                Field[x][y].closeCourse(x + 1, y + 1);  /*  \   */
                Field[x][y].closeCourse(x    , y + 1);  // |
                        /* ��������� ����������� ����� �� ���� ���� � ������ �����
                         * ������������ ���������� �����������, ������� ��������� */
            }

            if(y == 0) // ���� ������� ����
            {
                Field[x][y].closeCourse(x - 1, y    );   // _.
                Field[x][y].closeCourse(x - 1, y - 1);   //   \.
                Field[x][y].closeCourse(x    , y - 1);   //     !
                Field[x][y].closeCourse(x + 1, y - 1);   //      ./
                Field[x][y].closeCourse(x + 1, y    );   //        ._
                        /* ��������� ����������� ����� �� ���� ���� � ������� �����
                         * ������������ ���������� �����������, ������� ��������� */
            }

            if(y == Height) // ���� ������ ����
            {
                Field[x][y].closeCourse(x - 1, y    );  // -*
                Field[x][y].closeCourse(x - 1, y + 1);  //  /*
                Field[x][y].closeCourse(x    , y + 1);  //    |
                Field[x][y].closeCourse(x + 1, y + 1);  /*     *\   */
                Field[x][y].closeCourse(x + 1, y    );  //       *-
                        /* ��������� ����������� ����� �� ���� ���� � ������ �����
                         * ������������ ���������� �����������, ������� ��������� */
            }

            if((x == Width / 2) && (y == Height / 2))
            {           /* ���� ��� ����� �������� ����, �� ��� ����� ��� */
                Ball = &Field[x][y];
            }

            if(((x == Width) && (y == Height / 2)) ||
               ((x == 0)         && (y == Height / 2)))
            {           /* ���� ��� ����� �������� ������ ��� ������� ����, �� ��� ������*/
                Field[x][y].setGate(true);
            }
        }
    }
}

bool SockerField::isInvertStep(int x1, int y1, int x2, int y2)
{
    for(vector<QLine>::iterator it = Steps.begin();
        it != Steps.end(); ++it)
    {
        QLine step = *it;
        QLine invertStep(step.x2(), step.y2(), step.x1(), step.y1());

        if(QLine(x1, y1, x2, y2) == invertStep)
        {
            return true;
        }
    }
    return false;
}

void SockerField::newStep(int x, int y)
{
    Steps.push_back(QLine(Ball->getX(), Ball->getY(), x, y));
}

void SockerField::applyNewScene()
{
    for(vector<QLine>::iterator it = Steps.begin();
        it != Steps.end(); ++it)
    {
        /* ��������� �� ��������� ������ �����
         * ������ ��� ����� ��������� � �������� ����� */

        QLine line = *it;
        Field[line.x1()][line.y1()]
                .closeCourse(line.x2(), line.y2());

        Field[line.x2()][line.y2()]
                .closeCourse(line.x1(), line.y1());
                /* ������� ������ �� ��� ����������� ������
                 * ���� �� ����� */

    }
    Steps.clear();

    if(isTrap())
    {
        emit Victory();
    }
}

void SockerField::addStep(int x, int y)
{
    if(isValidCouse(x, y))
    {
        Steps.push_back(QLine(Ball->getX(), Ball->getY(), x, y));
        Ball = &Field[x][y];
        if(Ball->isGate())
        {
            emit Victory();
        }
    }
}

void SockerField::cancelLastStep()
{
    QLine lastStep = Steps.at(Steps.size() - 1);
    Ball = &Field[lastStep.x1()][lastStep.y1()];
    Steps.pop_back();

}

QLine SockerField::getLastStep()
{
    return Steps.back();
}

QPoint SockerField::getBallPosition()
{
    return QPoint(Ball->getX(), Ball->getY());
}

vector<QLine>* SockerField::getLines()
{
    Lines.clear();

    /* ��� ��� ��������� ������ ���� ����� �������� ���������, ��
     * ����� �������� ������ ������������ */

    for(int x = 0; x <= Width; x++)
    {
        for(int y = 0; y <= Height; y++)
        {
            vector<QLine> lines = Field[x][y].getLines();
            Lines.insert(Lines.end(),
                         lines.begin(),
                         lines.end());

        }
    }
    return &Lines;
}

vector<QLine>* SockerField::getSteps()
{
    return &Steps;
}

bool SockerField::isTrap()
{
    /* ��� ������ ���� ���������� �������� �������� �����������
     * ���� �� � ���������� ���� �� ���� ����� ���� �� �����
     * ������.
     * �� ������ ��� ������, ��� ��� ������� ��������� ����
     * �������� �� ������ */
    return false;

}

bool SockerField::isCancelStep(int x, int y)
{
    if(!Steps.empty())
    {
        QLine lastStep = Steps.at(Steps.size() - 1);
        return (QLine(x, y, Ball->getX(), Ball->getY()) == lastStep);
    }
    else
    {
        return false;
    }
}
