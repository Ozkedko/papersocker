#ifndef ABSTRACTPLAYER_H
#define ABSTRACTPLAYER_H

#include <QColor>
#include <QObject>
#include "types/sockerfield.h"

/*******************************�������� ������******************************
 *                                                                          *
 *      ������ ����� �������� ������� ������������ ������, �� �������� �����*
 *  ������ ��� ������(��, �������, ���� �� ���� � �������� ��� �����-����). *
 *      ���� ��� � ������ ������� ���, �� � ����������� ����� ���� ��� ���- *
 *  ���� �����������.                                                       *
 *                                                                          *
 ****************************************************************************/


class AbstractPlayer : public QObject
{
    Q_OBJECT
    //---------------------------------�����������-----------------------------------------
    public:
        AbstractPlayer(QObject* parent);
        ~AbstractPlayer();

    //---------------------------------����������------------------------------------------

    //---------------------------------����������------------------------------------------
    protected:
        SockerField* Field; // ��������� �� ����, �� ������� ����� ������� ����
        QString Name;       // ��� ������ ������
        QColor Color;       // ����, ������� ����� ���������� ���� ������ ������
        bool Activity;      // ���� ������� �� ������ �����

    //---------------------------------������----------------------------------------------
    public:
        void setField(SockerField* field);
        void setName(QString name);
        void setColor(QColor color);
        void setActivity(bool activity);
                /* ������� ��������� ���������� */

        SockerField* getField();
        QString getName();
        QColor getColor();
                /* ��������� ���������� ������ */

        virtual void BeginTurn(){}
                /* ������ ��� ��� */

    //---------------------------------�������---------------------------------------------
    signals:
        void addStep(int x, int y);
                /* ���� ����� ������ ��� */
        void sigEndOfTurn();

    //---------------------------------�����-----------------------------------------------
    public slots:
        virtual void clickReaction(int x, int y)
        {
            Q_UNUSED(x)
            Q_UNUSED(y)
        }
            /* ������� �� ���� */

        virtual void doubleClickReaction(){}
            /* ������� �� ������� ���� */
};

#endif // ABSTRACTPLAYER_H
