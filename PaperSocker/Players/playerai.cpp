#include "playerai.h"

PlayerAI::PlayerAI(QObject* parent):AbstractPlayer(parent)
{
    connect(&Timer, SIGNAL(timeout()), this, SLOT(slotStep()));
}

PlayerAI::~PlayerAI()
{

}

void PlayerAI::Analis()
{

}

void PlayerAI::BeginTurn()
{
    Analis();
    Timer.start(ANIMATION_SPEED_MS);
}

void PlayerAI::slotStep()
{
    if((MyCourse.count() > 0) && Activity)
    {
        Field->addStep(MyCourse.first().x2(), MyCourse.first().y2());
        MyCourse.removeFirst();
        if(!Field->isMoveAgain())
        {
            Field->applyNewScene();
            Timer.stop();
        }
    }
}

