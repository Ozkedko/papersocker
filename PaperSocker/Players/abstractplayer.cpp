#include "abstractplayer.h"

AbstractPlayer::AbstractPlayer(QObject* parent):QObject(parent)
{

}

AbstractPlayer::~AbstractPlayer()
{

}

void AbstractPlayer::setField(SockerField* field)
{
    Field = field;
}

void AbstractPlayer::setName(QString name)
{
    Name = name;
}

void AbstractPlayer::setColor(QColor color)
{
    Color = color;
}

void AbstractPlayer::setActivity(bool activity)
{
    Activity = activity;
}

SockerField* AbstractPlayer::getField()
{
    return Field;
}

QString AbstractPlayer::getName()
{
    return Name;
}

QColor AbstractPlayer::getColor()
{
    return Color;
}

