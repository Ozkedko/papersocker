#include "playerhuman.h"

PlayerHuman::PlayerHuman(QObject* parent):AbstractPlayer(parent)
{

}

PlayerHuman::~PlayerHuman()
{

}

void PlayerHuman::clickReaction(int x, int y)
{
    if(Activity)
    {
        if(Field->isCancelStep(x, y))
        {
            Field->cancelLastStep();
        }
        else
        {
            if(Field->isMoveAgain())
            {
                Field->addStep(x, y);
            }
        }
    }
}

void PlayerHuman::doubleClickReaction()
{
    if(Activity)
    {
        if(!Field->isMoveAgain())
        {
            Field->applyNewScene();
            emit sigEndOfTurn();
        }
    }
}

