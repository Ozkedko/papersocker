import QtQuick 2.0
import QtQuick.Controls 1.3

Item{
    id: playerSetting
    property string name: "player"
    property string type: "Human"
    property color color: "green"

    onColorChanged: rectName.color = color
    onNameChanged: lbPlayerTitle.text = name

    Rectangle{
        id: playerRect
        width: parent.width
        height: parent.height
        radius: 10
        border.color: playerSetting.color
        border.width: 2
        anchors.margins: 10

        Rectangle{
            id: rectName
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10

            width: 300
            height: 50

            color: playerSetting.color
            radius: 10

            Text{
                id: lbPlayerTitle
                anchors.fill: parent
                anchors.margins: 10

                text: name
                font.pointSize: 20
                color: "white"
            }
        }

        // Label "Enter name:"
        Text{
            id: lbName
            anchors.left: parent.left
            anchors.top: rectName.bottom
            anchors.margins: 10

            font.pointSize: 12

            text: "Enter name: "
        }

        // TextField of name
        TextField{
            id: teName
            anchors.left: parent.left
            anchors.top: lbName.bottom
            anchors.right: parent.right
            anchors.margins: 10
            height: 25

            onTextChanged: name = teName.text
        }

        // Label "Enter type of player"
        Text{
            id: lbPlayerType
            anchors.left: parent.left
            anchors.top: teName.bottom
            anchors.margins: 10

            font.pointSize: 12

            text: "Enter type of player: "
        }

        // ComboBox of types
        ComboBox{
            id: cbPlayerType
            anchors.left: parent.left
            anchors.top: lbPlayerType.bottom
            anchors.margins: 10

            enabled: false

            model: playerTypes
        }

        // Label "Select Color: "
        Text{
            id: lbPlayerColor
            anchors.left: parent.left
            anchors.top: cbPlayerType.bottom
            anchors.margins: 10

            font.pointSize: 12

            text: "Select color: "
        }
        // Grid of Colors
        GridView{
            id: colorView

            anchors.margins: 10
            anchors.top: lbPlayerColor.bottom
            anchors.left: parent.left
            anchors.right: parent.right


            cellWidth: 50
            width: 40
            model: playerColors

            delegate: Rectangle {
                width: 40
                height: 40
                anchors.margins: 10
                color: model.color
                border.color: "black"

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        if(referee.isAvailableColor(parent.color)){
                            playerSetting.color = parent.color
                        }
                    }
                    onEntered: parent.border.width = 2
                    onExited: parent.border.width = 1
                }
            }
        }
    }
}

