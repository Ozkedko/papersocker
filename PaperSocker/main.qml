import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

import QReferee 1.0

ApplicationWindow {
    id: appWindows
    title: qsTr("PaperSocker")
    width: 800
    height: 480
    visible: true

    ListModel{
        id: playerColors
        ListElement { color: "green"    }
        ListElement { color: "red"      }
        ListElement { color: "blue"     }
        ListElement { color: "yellow"   }
        ListElement { color: "pink"     }
        ListElement { color: "grey"     }
    }

    ListModel{
        id: playerTypes
        ListElement { type: "Human"     }
        ListElement { type: "AI"        }
        ListElement { type: "Remote"    }
    }

    ListModel{
        id: fieldTypes
        ListElement{ type: "AscetField" }
        ListElement{ type: "RnrField"   }
        ListElement{ type: "OtherField" }
    }

    Referee{
        id: referee

        onFieldWidthChanged: gameField.cField.changeStep()
        onFieldHeightChanged: gameField.cField.changeStep()
        onSigVictory: gameField.victory()
    }

    function onStart(){
        referee.fieldWidth = settingForm.fieldWidth
        referee.fieldHeight = settingForm.fieldHeight
        referee.fieldType = settingForm.fieldType

        referee.player1Color = settingForm.player1Color
        referee.player2Color = settingForm.player2Color

        referee.player1Name = settingForm.player1Name
        referee.player2Name = settingForm.player2Name

        referee.player1Type = settingForm.player1Type
        referee.player2Type = settingForm.player2Type

        settingForm.visible = false
        gameField.visible = true
        gameField.lbPlayer1Score.color = referee.player1Color
        gameField.lbPlayer2Score.color = referee.player2Color
        gameField.lbPlayer1Score.text = referee.player1Name
        gameField.lbPlayer2Score.text = referee.player2Name

        gameField.beginGame()
    }

    GameField{
        id: gameField
        anchors.fill: parent
        visible: false
        pbExit.onClicked: appWindows.close()
        pbPlayAgain.onClicked: onStart()
        pbMainMenu.onClicked: {
            settingForm.visible = true
            gameField.visible = false
        }
    }

    GameSetting{
        id: settingForm
        anchors.fill: parent

        pbExit.onClicked: appWindows.close()
        pbPlay.onClicked: onStart()
    }
}
