import QtQuick 2.0

Rectangle {
    id: gameField
    anchors.fill: parent

    property alias cField: cField
    property alias lbPlayer1Score: lbPlayer1Score
    property alias lbPlayer2Score: lbPlayer2Score

    property alias pbMainMenu: pbMainMenu
    property alias pbPlayAgain: pbPlayAgain
    property alias pbExit: pbExit

    function victory(){
        cField.visible = false
        lbVictory.visible = true
        lbVictory.color = referee.winnerColor
        lbWin.visible = true
        lbWin.text = referee.winnerName + " Win!"
        lbWin.color = referee.winnerColor
        pushButtonBox.color = referee.winnerColor
        pushButtonBox.goUnderWin()
    }

    function beginGame(){
        referee.startGame()
        pushButtonBox.goTop()
        gameField.cField.changeStep()
        cField.visible = true
        lbVictory.visible = false
        lbWin.visible = false
        cField.requestPaint()
    }

    Rectangle{
        id: pushButtonBox
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10
        height: 30

        property color color: "darkblue"

        function goTop(){
            pushButtonBox.color = "darkblue"
            pushButtonBox.anchors.top = gameField.top
            pushButtonBox.anchors.left = gameField.left
            pushButtonBox.anchors.right = gameField.right
            pushButtonBox.height = 30
        }

        function goUnderWin(){
            pushButtonBox.color = referee.winnerColor
            pushButtonBox.width = 600
            pushButtonBox.height = 100
            pushButtonBox.anchors.top = lbWin.bottom
        }

        onColorChanged: {
            pbPlayAgain.color = color
            pbExit.color = color
            pbMainMenu.color = color
        }

        SockerPushButton{
            id: pbPlayAgain
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width / 3
            height: parent.height
            color : parent.color
            text: "Play Again"

            onClicked: {
                beginGame()
            }
        }

        SockerPushButton{
            id: pbMainMenu
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width / 3
            height: parent.height
            color: parent.color
            text: "Main Menu"
        }

        SockerPushButton{
            id: pbExit
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width / 3
            height: parent.height
            color: parent.color
            text: "Exit"
        }
    }

    Rectangle {
        id: rectCanvas
        anchors.bottom: lbPlayer1Score.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: pushButtonBox.bottom
        anchors.margins: 10

        Canvas{
            id: cField
//            anchors.fill: parent

            property int wstep: {rectCanvas.width / referee.fieldWidth}
            property int hstep: {rectCanvas.height / referee.fieldHeight}

            property color winerColor: "white"

            property int xMouse: 0
            property int yMouse: 0
                    /* Название мне не нравится, но ничего лучше не придумал
                     * По наведению мыши будет рисоваться тонкий круг, а это
                     * его координаты */

            function changeStep(){
                wstep = Math.floor(rectCanvas.width / referee.fieldWidth)
                hstep = Math.floor(rectCanvas.height / referee.fieldHeight)

                cField.width = wstep * referee.fieldWidth
                cField.height = hstep * referee.fieldHeight
            }

            function drawGrid(){
                var gridlines = cField.getContext('2d');
                gridlines.clearRect(0, 0, cField.width, cField.height);
                gridlines.strokeStyle = "lightblue";
                gridlines.lineWidth = 1;
                gridlines.beginPath();

                for(var x = 0; x < cField.width; x += wstep){
                    gridlines.moveTo(x, 0);
                    gridlines.lineTo(x, cField.height);
                }

                for(var y = 0; y < cField.height; y += hstep){

                    gridlines.moveTo(0, y);
                    gridlines.lineTo(cField.width, y);
                }
                gridlines.stroke();
                gridlines.closePath();
                gridlines.restore();
            }

            function drawBorder() {
                // Рисуем края поля
                var borderlines = cField.getContext('2d');
                borderlines.strokeStyle = "darkblue";
                borderlines.lineWidth = 3;
                borderlines.beginPath();

                borderlines.moveTo(0, 0);
                borderlines.lineTo(cField.width, 0)
                borderlines.lineTo(cField.width, cField.height)
                borderlines.lineTo(0, cField.height)
                borderlines.lineTo(0, 0)
                borderlines.stroke();

                // Рисуем засечки, обозначающие середину поля

                borderlines.moveTo(cField.width / 2, 0)
                borderlines.lineTo(cField.width / 2, hstep / 4)
                borderlines.moveTo(cField.width / 2, cField.height)
                borderlines.lineTo(cField.width / 2, cField.height - (hstep / 4))
                borderlines.stroke();

                //Рисуем засечки, обозначающие ворота
                borderlines.moveTo(0, cField.height / 2 - hstep)
                borderlines.lineTo(wstep / 4, cField.height / 2 - hstep)
                borderlines.moveTo(0, cField.height / 2 + hstep)
                borderlines.lineTo(wstep / 4, cField.height / 2 + hstep)

                borderlines.moveTo(cField.width, cField.height / 2 - hstep)
                borderlines.lineTo(cField.width - (wstep / 4), cField.height / 2 - hstep)
                borderlines.moveTo(cField.width, cField.height / 2 + hstep)
                borderlines.lineTo(cField.width - (wstep / 4), cField.height / 2 + hstep)

                borderlines.stroke();
                borderlines.closePath();
                borderlines.restore();
            }

            function drawLines(){
                var lines = cField.getContext('2d');
                lines.strokeStyle = "darkblue";
                lines.lineWidth = 2;
                lines.beginPath();
                referee.firstLine()
                while(!referee.isEndOfLineVector()){
                    lines.moveTo(referee.currentX1Line * wstep, referee.currentY1Line * hstep);
                    lines.lineTo(referee.currentX2Line * wstep, referee.currentY2Line * hstep);
                    referee.nextLine()
                }
                lines.stroke();
                lines.closePath();
                lines.restore();
            }

            function drawSteps(){
                var steps = cField.getContext('2d');
                steps.strokeStyle = referee.getActualPlayerColor();
                steps.lineWidth = 2;
                steps.beginPath();
                referee.firstStep()
                while(!referee.isEndOfStepVector()){
                    referee.nextStep()
                    steps.moveTo(referee.currentX1Step * wstep, referee.currentY1Step * hstep);
                    steps.lineTo(referee.currentX2Step * wstep, referee.currentY2Step * hstep);
                }
                steps.stroke();
                steps.closePath();
                steps.restore();
            }

            function drawBall(){
                var ball = cField.getContext('2d');
                ball.fillStyle = referee.getActualPlayerColor();
                ball.lineWidth = 10;
                ball.beginPath();
                ball.ellipse(referee.ballX * wstep - 5, referee.ballY * hstep - 5, 10, 10)
                ball.fill();
                ball.closePath();
                ball.restore();
            }

            function drawMouseCircle(){
                var mousecircle = cField.getContext('2d');
                mousecircle.fillStyle = referee.getActualPlayerColor();
                mousecircle.lineWidth = 1;
                mousecircle.beginPath();
                mousecircle.ellipse(cField.xMouse - 5, cField.yMouse - 5, 10, 10)
                mousecircle.stroke();
                mousecircle.closePath();
                mousecircle.restore();
            }

            onPaint: {
                drawGrid();
                drawLines();
                drawBorder();
                drawSteps();
                drawBall();
                drawMouseCircle();
            }

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    referee.addStep(Math.round(mouseX / cField.wstep),
                                    Math.round(mouseY / cField.hstep));
                    cField.requestPaint();
                }

                onDoubleClicked: {
                    referee.onDoubleClick();
                    cField.xMouse = referee.ballX * cField.wstep;
                    cField.yMouse = referee.ballY * cField.hstep;
                    cField.requestPaint();
                }

                onMouseXChanged: {
                    if((Math.abs(Math.round(mouseX / cField.wstep) - referee.ballX) <= 1) &&
                       (Math.abs(Math.round(mouseY / cField.hstep) - referee.ballY) <= 1))
                    {
                        if(referee.isValidCourse(Math.round(mouseX / cField.wstep),
                                                 Math.round(mouseY / cField.hstep))){
                            cField.xMouse = Math.round(mouseX / cField.wstep) * cField.wstep
                        }
                    }
                }
                onMouseYChanged: {
                    if((Math.abs(Math.round(mouseX / cField.wstep) - referee.ballX) <= 1) &&
                       (Math.abs(Math.round(mouseY / cField.hstep) - referee.ballY) <= 1))
                    {
                        if(referee.isValidCourse(Math.round(mouseX / cField.wstep),
                                                 Math.round(mouseY / cField.hstep))){
                            cField.yMouse = Math.round(mouseY / cField.hstep) * cField.hstep
                        }
                        cField.requestPaint()
                    }
                }

            }
        }
    }


    Text{
        id: lbVictory
        anchors.centerIn: parent
        text: "Victory!!!"
        font.pointSize: 30
        visible: false
        color: cField.winerColor
    }

    Text{
        id: lbWin
        anchors.top: lbVictory.bottom
        anchors.horizontalCenter: lbVictory.horizontalCenter
        font.pointSize: 20
        visible: false
        color: cField.winerColor
    }

    Text {
        id: lbPlayer1Score
        y: 261
        text: qsTr(referee.player1Name)
        color: referee.player1Color
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        font.pixelSize: 20
    }

    Text {
        id: lbPlayer2Score
        x: 271
        y: 261
        text: qsTr(referee.player2Name)
        color: referee.player2Color
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        font.pixelSize: 20
    }
}

