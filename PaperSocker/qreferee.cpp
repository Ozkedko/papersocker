#include "qreferee.h"

QReferee::QReferee(QObject* parent):QObject(parent)
{
    setDefaultValues();
}
QReferee::~QReferee()
{

}
QString QReferee::getPlayer2Type() const
{
    switch(player2Type)
    {
        case SockerTypes::AIPlayer : return "AI";
        case SockerTypes::RemotePlayer : return "Remote";
        default : return "Human";
    }
}

void QReferee::setPlayer2Type(const QString& value)
{
    if(value == "Human")
    {
        player2Type = SockerTypes::HumanPlayer;
    }
    else
    {
        if(value == "AI")
        {
            player2Type = SockerTypes::AIPlayer;
        }
        else
        {
            player2Type = SockerTypes::RemotePlayer;
        }
    }
}

QString QReferee::getPlayer1Type() const
{
    switch(player1Type)
    {
        case SockerTypes::AIPlayer : return "AI";
        case SockerTypes::RemotePlayer : return "Remote";
        default : return "Human";
    }
}

void QReferee::setPlayer1Type(const QString& value)
{
    if(value == "Human")
    {
        player1Type = SockerTypes::HumanPlayer;
    }
    else
    {
        if(value == "AI")
        {
            player1Type = SockerTypes::AIPlayer;
        }
        else
        {
            player1Type = SockerTypes::RemotePlayer;
        }
    }
}

int QReferee::getFieldHeight() const
{
    return fieldHeight;
}

void QReferee::setFieldHeight(int value)
{
    fieldHeight = value;
}

bool QReferee::isEndOfLineVector()
{
    return ((LineIterator == LineVector.end()) || (LineVector.empty()));
}

void QReferee::nextLine()
{
    if(!isEndOfLineVector())
    {
        currentLine = *LineIterator++;
    }
}

void QReferee::firstLine()
{
    LineVector = *Field->getLines();
    LineIterator = LineVector.begin();
}

bool QReferee::isEndOfStepVector()
{
    return (StepIterator == StepVector.end());
}

void QReferee::nextStep()
{
    if(!isEndOfStepVector())
    {
        currentStep = *StepIterator++;
    }
}

void QReferee::firstStep()
{
    StepVector = *Field->getSteps();
    StepIterator = StepVector.begin();
}

void QReferee::onDoubleClick()
{
    actualPlayer->doubleClickReaction();
}

bool QReferee::isAvailableColor(const QColor& color)
{
    if(PlayerList.isEmpty())
    {
        return ((!(color == player1Color)) && (!(color == player2Color)));
    }
    else
    {
        return ((!(color == Player1->getColor())) && (!(color == Player2->getColor())));
    }
}

int QReferee::getX1Line()
{
    return currentLine.x1();
}

int QReferee::getY1Line()
{
    return currentLine.y1();
}

int QReferee::getX2Line()
{
    return currentLine.x2();
}

int QReferee::getY2Line()
{
    return currentLine.y2();
}

int QReferee::getX1Step()
{
    return currentStep.x1();
}

int QReferee::getY1Step()
{
    return currentStep.y1();
}

int QReferee::getX2Step()
{
    return currentStep.x2();
}

int QReferee::getY2Step()
{
    return currentStep.y2();
}

int QReferee::getBallX()
{
    return Field->getBallPosition().x();
}

int QReferee::getBallY()
{
    return Field->getBallPosition().y();
}

QColor QReferee::getWinnerColor() const
{
    return whoWin()->getColor();
}

QString QReferee::getWinnerName() const
{
    return whoWin()->getName();
}

AbstractPlayer* QReferee::whoWin() const
{
    if(Field->getBallPosition().x() == 0){
        return Player2;
    }
    if(Field->getBallPosition().x() == Field->getWidth()){
        return Player1;
    }
    return actualPlayer;
}

void QReferee::addStep(int x, int y)
{
    // ���� ��� ������ �� �������� �����.
    if((abs(x - Field->getBallPosition().x()) <= 1) &&
       (abs(y - Field->getBallPosition().y()) <= 1))
    {
        actualPlayer->clickReaction(x, y);
    }
}

QColor QReferee::getActualPlayerColor() const
{
    return actualPlayer->getColor();
}

QString QReferee::getActualPlayerName() const
{
    return actualPlayer->getName();
}

QPoint QReferee::getBallPosition() const
{
    return Field->getBallPosition();
}

int QReferee::getFieldWidth() const
{
    return fieldWidth;
}

void QReferee::setFieldWidth(int value)
{
    fieldWidth = value;
}

QString QReferee::getFieldType() const
{
    switch(fieldType)
    {
        case SockerTypes::AscetField : return "AscetField";
        case SockerTypes::RnrField :   return "RnrField";
        default : return "OtherField";
    }
}

void QReferee::setFieldType(const QString& value)
{
    if(value == "AscetField")
    {
        fieldType = SockerTypes::AscetField;
    }
    else
    {
        if(value == "RnrField")
        {
            fieldType = SockerTypes::RnrField;
        }
        else
        {
            fieldType = SockerTypes::OtherField;
        }
    }
}

QString QReferee::getPlayer2Name() const
{
    return player2Name;
}

void QReferee::setPlayer2Name(const QString& value)
{
    player2Name = value;
}

QString QReferee::getPlayer1Name() const
{
    return player1Name;
}

void QReferee::setPlayer1Name(const QString& value)
{
    player1Name = value;
}

QColor QReferee::getPlayer2Color() const
{
    return player2Color;
}

void QReferee::setPlayer2Color(const QColor& value)
{
    player2Color = value;
}

QColor QReferee::getPlayer1Color() const
{
    return player1Color;
}

void QReferee::setPlayer1Color(const QColor& value)
{
    player1Color = value;
}

void QReferee::startGame()
{
    qDebug() << "PLAYER 1:\t"<< player1Name << player1Color << getPlayer1Type();
    qDebug() << "PLAYER 2:\t"<< player2Name << player2Color << getPlayer2Type();
    qDebug() << "FIELD:\t"<< getFieldType() << fieldWidth << fieldHeight;

    // �������� ����
    if(Field != NULL)
    {
        delete Field;
    }

    if(fieldType != SockerTypes::OtherField)
    {
        Field = new SockerField(fieldType);
        fieldWidth = Field->getWidth();
        fieldHeight = Field->getHeight();
    }
    else
    {
        Field = new SockerField(fieldWidth, fieldHeight);
    }

    connect(Field, Field->Victory, this, sigVictory);

    // �������� �������
    PlayerList.clear();
    switch(player1Type)
    {
        case SockerTypes::HumanPlayer :  PlayerList.append(new PlayerHuman(this)); break;
        case SockerTypes::AIPlayer :     PlayerList.append(new PlayerAI(this));    break;
        case SockerTypes::RemotePlayer : PlayerList.append(new PlayerRemote(this));break;
    }
    Player1->setActivity(true);
    Player1->setColor(player1Color);
    Player1->setName(player1Name);
    Player1->setField(Field);

    switch(player2Type)
    {
        case SockerTypes::HumanPlayer :  PlayerList.append(new PlayerHuman(this)); break;
        case SockerTypes::AIPlayer :     PlayerList.append(new PlayerAI(this));    break;
        case SockerTypes::RemotePlayer : PlayerList.append(new PlayerRemote(this));break;
    }

    Player2->setActivity(false);
    Player2->setColor(player2Color);
    Player2->setName(player2Name);
    Player2->setField(Field);

    connect(Player1, AbstractPlayer::sigEndOfTurn, this, QReferee::slotEndOfTurn);
    connect(Player2, AbstractPlayer::sigEndOfTurn, this, QReferee::slotEndOfTurn);

    actualPlayer = Player1;
}


bool QReferee::isValidCourse(int x, int y)
{
    return ((Field->isValidCouse(x, y) && Field->isMoveAgain()) || Field->isCancelStep(x, y));
}

void QReferee::applyLineScene()
{

}

void QReferee::setDefaultValues()
{
    Field = NULL;
    player1Color = QColor("green");
    player2Color = QColor("red");
    player1Name = "Player1";
    player2Name = "Player2";
    fieldWidth = 20;
    fieldHeight = 10;
    fieldType = SockerTypes::AscetField;
}

void QReferee::slotEndOfTurn()
{
    if(actualPlayer == Player1)
    {
        actualPlayer = Player2;
        Player1->setActivity(false);
        Player2->setActivity(true);
    }
    else
    {
        actualPlayer = Player1;
        Player1->setActivity(true);
        Player2->setActivity(false);
    }
}
