import QtQuick 2.0
import QtQuick.Controls 1.3

Item{
    id: fieldSetting
    property string type: "AscetField"
    property int fieldWidth: 20
    property int fieldHeight: 10
    width: 500
    height: 100

    Rectangle{
        id: fieldRect
        anchors.fill: parent
        border.color: "grey"
        border.width: 2
        radius: 10

        // Label "Type of Field:"
        Text {
            id: lbFieldType
            text: qsTr("Select Type of Field:")
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 10

            font.pointSize: 12
        }

        // ComboBox of types of field
        ComboBox{
            id: cbFieldType
            anchors.left: parent.left
            anchors.top: lbFieldType.bottom
            anchors.margins: 10

            model: fieldTypes

            onCurrentTextChanged:{
                type = currentText
                otherSettingRect.visible = (currentIndex == 2);
            }
        }
        // Other Setting
        Rectangle{
            id: otherSettingRect
            anchors.left: lbFieldType.right
            anchors.right: fieldRect.right
            anchors.top: fieldRect.top
            anchors.bottom: fieldRect.bottom
            anchors.margins: 10

            border.color: "grey"
            border.width: 2
            radius: 10

            visible: false

            // Label Enter width of field
            Text {
                id: lbFieldWidth
                text: qsTr("Enter Width:")

                anchors.left: otherSettingRect.left
                anchors.margins: 10
                anchors.verticalCenter: parent.verticalCenter

                font.pointSize: 12
            }

            // TextEdit of width of field
            SpinBox{
                id: sbFieldWidth
                anchors.left: lbFieldWidth.right
                anchors.top: lbFieldWidth.top
                anchors.margins: 10

                width: 50
                height: 20
                anchors.leftMargin: 6
                anchors.topMargin: 0
                value: 20
                stepSize: 2

                onValueChanged: fieldSetting.fieldWidth = value
            }

            // Label enter height of field
            Text {
                id: lbFieldHeight
                text: qsTr("Enter Height:")
                anchors.top: lbFieldWidth.top
                anchors.left: sbFieldWidth.right
                anchors.leftMargin: 10
                anchors.rightMargin: 10

                font.pointSize: 12
            }

            // TextEdit of height of field
            SpinBox{
                id: cbFieldHeight
                anchors.left: lbFieldHeight.right
                anchors.top: lbFieldHeight.top
                anchors.margins: 10

                width: 50
                height: 20

                anchors.leftMargin: 6
                anchors.topMargin: 0
                value: 10
                stepSize: 2

                onValueChanged: fieldSetting.fieldHeight = value
            }
        }
    }
}
