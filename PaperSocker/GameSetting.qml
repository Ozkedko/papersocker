import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

Rectangle{
    id: settingField
    anchors.fill: parent
    property alias pbPlay: pbPlay
    property alias pbExit: pbExit

    property alias fieldWidth : fieldSetting.fieldWidth
    property alias fieldHeight : fieldSetting.fieldHeight
    property alias fieldType: fieldSetting.type

    property alias player1Name : player1.name
    property alias player2Name : player2.name

    property alias player1Type : player1.type
    property alias player2Type : player2.type

    property alias player1Color: player1.color
    property alias player2Color: player2.color


    // Player #1
    PlayerSetting{
        id: player1
        anchors.left: parent.left
        width: (parent.width / 2) - 5
        height: parent.height - 100
        name: referee.player1Name
        color: referee.player1Color

        onColorChanged: {
            referee.player1Color = color
        }
    }

    // Player #2
    PlayerSetting{
        id: player2
        anchors.right: parent.right
        width: (parent.width / 2) - 5
        height: parent.height - 100
        name: referee.player2Name
        color: referee.player2Color

        onColorChanged: {
            referee.player2Color = color
        }
    }

    // Field Setting
    FieldSetting{
        id: fieldSetting
        anchors.top: player1.bottom
        anchors.bottom: parent.bottom
        anchors.margins: 10
        width: 650
    }

    //Кнопка
    SockerPushButton {
        id: pbPlay //Имя кнопки
        anchors.left: fieldSetting.right
        anchors.top: player1.bottom
        anchors.right: parent.right
        anchors.margins: 10
        height: (settingField.height - player2.height - 30) / 2
        radius: 10
        text: "Play"
        color: "green"
    }

    SockerPushButton {
        id: pbExit //Имя кнопки
        anchors.left: fieldSetting.right
        anchors.top: pbPlay.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        radius: 10
        text: "Exit"
        color: "red"
    }
}
