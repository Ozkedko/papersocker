TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    qreferee.cpp \
    types/sockerfield.cpp \
    types/sockerpoint.cpp \
    Players/abstractplayer.cpp \
    Players/playerai.cpp \
    Players/playerhuman.cpp \
    Players/playerremote.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    types/sockertypes.h \
    qreferee.h \
    types/sockerfield.h \
    types/sockerpoint.h \
    Players/abstractplayer.h \
    Players/playerai.h \
    Players/playerhuman.h \
    Players/playerremote.h
