#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "qreferee.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<QReferee>("QReferee", 1, 0, "Referee");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    return app.exec();
}
