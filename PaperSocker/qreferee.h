#ifndef REFEREE_H
#define REFEREE_H

#include <QObject>
#include <QColor>
#include <QDebug>
#include <QList>
#include <QLine>
#include <types/sockertypes.h>
#include <types/sockerfield.h>
#include <Players/abstractplayer.h>
#include <Players/playerai.h>
#include <Players/playerhuman.h>
#include <Players/playerremote.h>
#include <vector>

#define Player1 PlayerList.at(0)
#define Player2 PlayerList.at(1)

/*****************************�������� ������****************************
 *                                                                      *
 *      ������ ����� �������� ������� ����� - ������ �� ���������� ��-  *
 *  �������� �����, ������ ����, �������, ������� ����� ���� �� ������*
 *  ������ � �������, � ��� �� ������ ����� ���� ����� ������������ ��- *
 *  ����� �++ ����� � QML ������ �������.                               *
 *                                                                      *
 ************************************************************************/

using namespace std;

using namespace SockerTypes;

class QReferee: public QObject
{
    Q_OBJECT
    Q_ENUMS(FieldTypes)
    Q_ENUMS(PlayerTypes)

    Q_PROPERTY(QString player1Name READ getPlayer1Name WRITE setPlayer1Name NOTIFY player1NameChanged)
    Q_PROPERTY(QString player2Name READ getPlayer2Name WRITE setPlayer2Name NOTIFY player2NameChanged)
    Q_PROPERTY(QColor player1Color READ getPlayer1Color WRITE setPlayer1Color NOTIFY player1ColorChanged)
    Q_PROPERTY(QColor player2Color READ getPlayer2Color WRITE setPlayer2Color NOTIFY player2ColorChanged)
    Q_PROPERTY(QString fieldType READ getFieldType WRITE setFieldType NOTIFY fieldTypeChanged)
    Q_PROPERTY(QString player1Type READ getPlayer1Type WRITE setPlayer1Type NOTIFY player1TypeChanged)
    Q_PROPERTY(QString player2Type READ getPlayer2Type WRITE setPlayer2Type NOTIFY player2TypeChanged)
    Q_PROPERTY(int fieldWidth READ getFieldWidth WRITE setFieldWidth NOTIFY fieldWidthChanged)
    Q_PROPERTY(int fieldHeight READ getFieldHeight WRITE setFieldHeight NOTIFY fieldHeightChanged)
    Q_PROPERTY(int ballX READ getBallX)
    Q_PROPERTY(int ballY READ getBallY)

    Q_PROPERTY(int currentX1Line READ getX1Line)
    Q_PROPERTY(int currentY1Line READ getY1Line)
    Q_PROPERTY(int currentX2Line READ getX2Line)
    Q_PROPERTY(int currentY2Line READ getY2Line)

    Q_PROPERTY(int currentX1Step READ getX1Step)
    Q_PROPERTY(int currentY1Step READ getY1Step)
    Q_PROPERTY(int currentX2Step READ getX2Step)
    Q_PROPERTY(int currentY2Step READ getY2Step)

    Q_PROPERTY(QColor winnerColor READ getWinnerColor)
    Q_PROPERTY(QString winnerName READ getWinnerName)

    //---------------------------------�����������------------------------------------------
    public:
        QReferee(QObject* parent = 0);
        ~QReferee();

    //---------------------------------����������-------------------------------------------
    //---------------------------------����������-------------------------------------------
    private:
        QColor player1Color;
        QColor player2Color;
        QString player1Name;
        QString player2Name;

        int fieldWidth;
        int fieldHeight;

        vector<QLine> StepVector;
        vector<QLine>::iterator StepIterator;
        QLine currentLine;

        vector<QLine> LineVector;
        vector<QLine>::iterator LineIterator;
        QLine currentStep;

        SockerField* Field;

        SockerTypes::FieldTypes fieldType;
        SockerTypes::PlayerTypes player1Type;
        SockerTypes::PlayerTypes player2Type;

        QList<AbstractPlayer*> PlayerList;
        AbstractPlayer* actualPlayer;

    //---------------------------------������-----------------------------------------------
    public:
        Q_INVOKABLE void addStep(int x, int y);
        Q_INVOKABLE QColor getActualPlayerColor() const;
        Q_INVOKABLE QString getActualPlayerName() const;
        Q_INVOKABLE QPoint getBallPosition() const;
        Q_INVOKABLE void startGame();
        Q_INVOKABLE bool isValidCourse(int x, int y);
        Q_INVOKABLE void applyLineScene();

        Q_INVOKABLE bool isEndOfLineVector();
        Q_INVOKABLE void nextLine();
        Q_INVOKABLE void firstLine();

        Q_INVOKABLE bool isEndOfStepVector();
        Q_INVOKABLE void nextStep();
        Q_INVOKABLE void firstStep();

        Q_INVOKABLE void onDoubleClick();

        Q_INVOKABLE bool isAvailableColor(const QColor& color);

        void setDefaultValues();

        QColor getPlayer1Color() const;
        void setPlayer1Color(const QColor& value);

        QColor getPlayer2Color() const;
        void setPlayer2Color(const QColor& value);

        QString getPlayer1Name() const;
        void setPlayer1Name(const QString& value);

        QString getPlayer2Name() const;
        void setPlayer2Name(const QString& value);

        QString getFieldType() const;
        void setFieldType(const QString& value);

        int getFieldWidth() const;
        void setFieldWidth(int value);

        int getFieldHeight() const;
        void setFieldHeight(int value);

        QString getPlayer1Type() const;
        void setPlayer1Type(const QString& value);

        QString getPlayer2Type() const;
        void setPlayer2Type(const QString& value);

        int getX1Line();
        int getY1Line();
        int getX2Line();
        int getY2Line();

        int getX1Step();
        int getY1Step();
        int getX2Step();
        int getY2Step();

        int getBallX();
        int getBallY();

        QColor getWinnerColor() const;
        QString getWinnerName() const;

    private:
        AbstractPlayer* whoWin() const;

    //---------------------------------�������----------------------------------------------
    signals:
        void player1NameChanged(const QString& name);
        void player2NameChanged(const QString& name);
        void player1ColorChanged(const QColor& color);
        void player2ColorChanged(const QColor& color);
        void fieldTypeChanged(const QString& type);
        void fieldWidthChanged(const int width);
        void fieldHeightChanged(const int height);
        void lineListChanged(const QList<QLine>& linelist);
        void player1TypeChanged();
        void player2TypeChanged();

        void sigStartGame();
        void sigVictory();

    //---------------------------------�����------------------------------------------------
    public slots:
        void slotEndOfTurn();
};

#endif // REFEREE_H
