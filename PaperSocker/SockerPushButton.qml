import QtQuick 2.0

Item{
    id: pushButton
    width: 200
    height: 100

    property color color: "green"
    property string text: "Push Button"
    property int borderWidth: 3
    property int radius: 20
    signal clicked

    onColorChanged: pbBody.color = color

    Rectangle{
        id: pbBody
        anchors.fill: parent
        radius: parent.radius
        color: parent.color
        border.color: "white"
        border.width: parent.borderWidth

        Text{
            id: lbTitle
            anchors.centerIn: parent
            color: "white"
            font.pixelSize: 25
            visible: parent.visible
            text: pushButton.text
        }

        MouseArea{
            anchors.fill: parent
            hoverEnabled: true

            onEntered: {
                parent.color = "white"
                parent.border.color = pushButton.color
                lbTitle.color = pushButton.color
            }

            onExited: {
                parent.color = pushButton.color
                parent.border.color = "white"
                lbTitle.color = "white"
            }

            onPressed: {
                exited()
            }

            onReleased: {
                entered()
            }

            onClicked: {
                pushButton.clicked()
            }
        }
    }
}
